#include "../include/board.h"

using namespace std;

/******************************************************************************/
/*******************************BOARD METHODS**********************************/
/******************************************************************************/

board::board (int l_board_id, string l_board_vendor, string l_board_model, string l_board_serial) {
    board_id     = l_board_id;
    board_vendor = l_board_vendor;
    board_model  = l_board_model;
    board_serial = l_board_serial;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: New board with ID: " << board_id << " - " << board_vendor << " mod. " << board_model << " - Serial: " << board_serial << endl;
    #endif
}

board::~board (void) {
    map<uint8_t, digital_pin*>::iterator it_digital;
    map<uint8_t, analog_pin*>::iterator it_analog;
    for(it_digital = digital_pins.begin(); it_digital != digital_pins.end(); it_digital++){
      cout << "LOG: Deleting Digital pin " << it_digital->second->get_pin_id() << endl;
      delete it_digital->second;
    }
    for(it_analog = analog_pins.begin(); it_analog != analog_pins.end(); it_analog++){
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Deleting Analog pin " << it_analog->second->get_pin_id() << endl;
      #endif
      delete it_analog->second;
    }
    digital_pins.clear();
    analog_pins.clear();
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Delete board" << endl;
    #endif
}

bool board::board_init(void){
    init_success = true;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Board init" << endl;
    #endif
    #if DEMO != 1
    if (wiringPiSetup () == -1){
      init_success = false;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Board init FAILED!!" << endl;
      #endif
    }
    #endif
    return init_success;
}

int8_t  board::digital_pin_add(int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, uint8_t l_pin_value){
    int8_t ret_value = STATUS_ERROR;

    digital_pin *p_pin = NULL;
    pair <map<uint8_t, digital_pin*>::iterator, bool> ret;

    p_pin = new digital_pin(l_pin_id, l_pin_name, l_pin_mode, l_pin_value);
    if(p_pin != NULL){
      ret = digital_pins.insert(pair<uint8_t, digital_pin*>(l_pin_id, p_pin));
      if(!ret.second){
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Digital Pin " << (int)l_pin_id << " already exists. Pin ID must be unique." << endl;
        #endif
        delete p_pin;
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Add new Digital Pin - ID: " << (int)l_pin_id << " NAME: " << l_pin_name << endl;
        #endif
        ret_value = STATUS_OK;
      }
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Unable to create new Pin." << endl;
      #endif
    }
    return ret_value;
}

int8_t  board::digital_pin_remove(int16_t l_pin_id){
    int8_t ret_value = STATUS_OK;
    if(digital_pins.find(l_pin_id) != digital_pins.end()){
      delete digital_pins.at(l_pin_id);
      digital_pins.erase(l_pin_id);
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital Pin " << (int)l_pin_id << " removed" << endl;
      #endif
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital pin " << (int)l_pin_id << " NOT found!" << endl;
      #endif
      ret_value = STATUS_ERROR;
    }
    return ret_value;
}

int8_t  board::digital_pin_set_mode(uint8_t l_pin_id, uint8_t l_pin_mode){
    uint8_t ret_value = STATUS_OK;
    if(digital_pins.find(l_pin_id) != digital_pins.end()){
      if(digital_pins.at(l_pin_id)->set_pin_mode(l_pin_mode) == l_pin_mode){
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Digital Pin " << (int)l_pin_id << " set to mode " << (l_pin_mode == DPIN_MODE_IN ? "IN" : "OUT") << endl;
        #endif
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Digital pin " << (int)l_pin_id << " cannot be set to " << (l_pin_mode == DPIN_MODE_IN ? "IN" : "OUT") << endl;
        #endif
        ret_value = STATUS_ERROR;
      }
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital pin " << (int)l_pin_id << " NOT found!" << endl;
      #endif
      ret_value = STATUS_ERROR;
    }
    return ret_value;
}

uint8_t  board::analog_pin_add(int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, uint16_t l_pin_value){
    uint8_t ret_value = STATUS_ERROR;
    analog_pin *p_pin = NULL;
    pair <map<uint8_t, analog_pin*>::iterator, bool> ret;

    p_pin = new analog_pin(l_pin_id, l_pin_name, l_pin_mode, l_pin_value);
    if(p_pin != NULL){
      ret = analog_pins.insert(pair<uint8_t, analog_pin*>(l_pin_id, p_pin));
      if(!ret.second){
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Analog Pin " << (int)l_pin_id << " already exists. Pin ID must be unique." << endl;
        #endif
        delete p_pin;
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Add new Analog Pin - ID: " << (int)l_pin_id << " NAME: " << l_pin_name << endl;
        #endif
        ret_value = STATUS_OK;
      }
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Unable to create new Pin." << endl;
      #endif
    }
    return ret_value;
}

uint8_t  board::analog_pin_remove(int16_t l_pin_id){
    uint8_t ret_value = STATUS_OK;

    if(analog_pins.find(l_pin_id) != analog_pins.end()){
      delete analog_pins.at(l_pin_id);
      analog_pins.erase(l_pin_id);
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Aigital Pin " << (int)l_pin_id << " removed" << endl;
      #endif
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Analog pin " << (int)l_pin_id << " NOT found!" << endl;
      #endif
      ret_value = STATUS_ERROR;
    }
    return ret_value;
}

int8_t  board::analog_pin_set_mode(uint8_t l_pin_id, uint8_t l_pin_mode){
    uint8_t ret_value = STATUS_OK;
    if(analog_pins.find(l_pin_id) != analog_pins.end()){
      if(analog_pins.at(l_pin_id)->set_pin_mode(l_pin_mode) == l_pin_mode){
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Analog Pin " << (int)l_pin_id << " set to mode " << (l_pin_mode == APIN_MODE_IN ? "IN" : "OUT") << endl;
        #endif
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Analog pin " << (int)l_pin_id << " cannot be set to " << (l_pin_mode == APIN_MODE_IN ? "IN" : "OUT") << endl;
        #endif
        ret_value = STATUS_ERROR;
      }
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Analog pin " << (int)l_pin_id << " NOT found!" << endl;
      #endif
      ret_value = STATUS_ERROR;
    }
    return ret_value;
}

int8_t board::digital_read(uint8_t l_pin_id){
    int8_t ret_value = STATUS_ERROR;

    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Digital Pin RD" << endl;
    #endif
    if(digital_pins.find(l_pin_id) != digital_pins.end()){
      ret_value = (int8_t) ((digital_pins.at(l_pin_id))->digital_read());
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital pin " << (int)l_pin_id << " not found!" << endl;
      #endif
    }
    return ret_value;
}

int8_t board::digital_write(uint8_t l_pin_id, bool l_pin_value){
    int8_t ret_value = STATUS_OK;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Digital Pin WR"  << endl;
    #endif
    if(digital_pins.find(l_pin_id) != digital_pins.end()){
      if((digital_pins.at(l_pin_id))->get_pin_mode() == DPIN_MODE_OUT){
        if((digital_pins.at(l_pin_id))->digital_write(l_pin_value)){
            ret_value = STATUS_OK;
        }
        else {
          ret_value = STATUS_ERROR;
          # if LOG_LEVEL > LOG_LEVEL_NO
            cout << "LOG: Digital pin " << (int)l_pin_id << " cannot be written!" << endl;
          #endif
        }
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Digital pin " << (int)l_pin_id << " is not configured as OUTPUT!" << endl;
        #endif
        ret_value = STATUS_ERROR;
      }
    } else{
      ret_value = STATUS_ERROR;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital pin " << (int)l_pin_id << " not found!" << endl;
      #endif
    }
    return ret_value;
}

int8_t board::digital_toggle(uint8_t l_pin_id){
    int8_t ret_value = STATUS_ERROR;

    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Digital Pin Toggle" << endl;
    #endif
    if(digital_pins.find(l_pin_id) != digital_pins.end()){
      if((digital_pins.at(l_pin_id))->get_pin_mode() == DPIN_MODE_OUT){
        if((digital_pins.at(l_pin_id))->digital_toggle()){
            ret_value = STATUS_OK;
        }
        else {
          ret_value = STATUS_ERROR;
          # if LOG_LEVEL > LOG_LEVEL_NO
            cout << "LOG: Digital pin " << (int)l_pin_id << " cannot be written!" << endl;
          #endif
        }
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Digital pin " << (int)l_pin_id << " is not configured as OUTPUT!" << endl;
        #endif
        ret_value = STATUS_ERROR;
      }
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital pin " << (int)l_pin_id << " NOT found!" << endl;
      #endif
      ret_value = STATUS_ERROR;
    }
    return ret_value;
}

uint16_t board::analog_read(uint8_t l_pin_id){
    uint16_t ret_value = AREAD_ERROR;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Analog Pin RD (DUMMY)" << endl;
    #endif
    if(analog_pins.find(l_pin_id) != analog_pins.end()){
      ret_value = (uint16_t) ((analog_pins.at(l_pin_id))->analog_read());
    }
    else {
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Analog pin " << (int)l_pin_id << " NOT found!" << endl;
      #endif
    }
    return ret_value;
}

uint16_t board::analog_write(uint8_t l_pin_id, uint16_t l_pin_value){
    uint16_t ret_value = AWRITE_ERROR;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Analog Pin WR (DUMMY)" << endl;
    #endif
    if(analog_pins.find(l_pin_id) != analog_pins.end()){
      if((analog_pins.at(l_pin_id))->get_pin_mode() == APIN_MODE_OUT){
        ret_value = (analog_pins.at(l_pin_id))->analog_write(l_pin_value);
      }
      else{
        # if LOG_LEVEL > LOG_LEVEL_NO
          cout << "LOG: Analog pin " << (int)l_pin_id << " is not configured as OUTPUT!" << endl;
        #endif
      }
    }
    else{
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Analog pin " << (int)l_pin_id << " not found!" << endl;
      #endif
    }
    return ret_value;
}

/******************************************************************************/
/*****************************DIGITAL PIN METHODS******************************/
/******************************************************************************/

digital_pin::digital_pin (int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, bool l_pin_value) {
    pin_id = l_pin_id;
    pin_name = l_pin_name;
    pin_mode = l_pin_mode;
    pin_value = l_pin_value;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: New Digital Pin " << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == DPIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
    #endif
    set_pin_mode(l_pin_mode);
}

digital_pin::~digital_pin (void) {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Delete Digital Pin " << endl;
    #endif
}

int16_t   digital_pin::get_pin_id(void){
    return pin_id;
}

string    digital_pin::get_pin_name(void){
    return pin_name;
}

uint8_t   digital_pin::get_pin_mode(void){
    return pin_mode;
}

uint8_t   digital_pin::set_pin_mode(uint8_t l_pin_mode){
    pin_mode = l_pin_mode;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Digital Pin SET:" << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == DPIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
    #endif
    #if DEMO != 1
      if (pin_mode == DPIN_MODE_IN){
      	pinMode(pin_id, INPUT);		//actual wiringPi constants
      } else {
	pinMode(pin_id, OUTPUT);
      }
    #endif
    return pin_mode;
}

bool digital_pin::digital_read(void){
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Digital Pin RD:" << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == DPIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
    #endif
    #if DEMO != 1
      pin_value = digitalRead (pin_id);
    #endif
    return pin_value;
}

bool digital_pin::digital_write(bool l_pin_value){
    bool ret_value = true;
    if(pin_mode == DPIN_MODE_OUT){
      pin_value = l_pin_value;
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital Pin WR:" << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == DPIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
      #endif
      #if DEMO != 1
        digitalWrite (pin_id, pin_value);
      #endif
    } else {
      ret_value = false;
      #if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital Pin set as INPUT cannot perform WRITE" << endl;
      #endif
    }
    return ret_value;
}

bool digital_pin::digital_toggle(void){
    bool ret_value = true;
    if(pin_mode == DPIN_MODE_OUT){
      if(pin_value == DVALUE_LOW){
        pin_value = DVALUE_HIGH;
      }
      else {
        pin_value = DVALUE_LOW;
      }
      # if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital Pin WR:" << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == DPIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
      #endif
      #if DEMO != 1
        digitalWrite (pin_id, pin_value);
      #endif
    } else {
      ret_value = false;
      #if LOG_LEVEL > LOG_LEVEL_NO
        cout << "LOG: Digital Pin set as INPUT cannot perform WRITE" << endl;
      #endif
    }
    return ret_value;
}

/******************************************************************************/
/******************************ANALOG PIN METHODS******************************/
/******************************************************************************/

analog_pin::analog_pin (int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, uint16_t l_pin_value) {
    pin_id = l_pin_id;
    pin_name = l_pin_name;
    pin_mode = l_pin_mode;
    pin_value = l_pin_value;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: New Analog Pin " << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == APIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
    #endif
}

analog_pin::~analog_pin (void) {
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Delete Analog Pin " << endl;
    #endif
}

int16_t   analog_pin::get_pin_id(void){
    return pin_id;
}

string    analog_pin::get_pin_name(void){
    return pin_name;
}

uint8_t   analog_pin::get_pin_mode(void){
    return pin_mode;
}

uint8_t   analog_pin::set_pin_mode(uint8_t l_pin_mode){
    pin_mode = l_pin_mode;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Analog Pin SET:" << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == APIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
    #endif
    return pin_mode;
}

uint16_t  analog_pin::analog_read(void){
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Analog Pin RD:" << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == APIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
    #endif
    return pin_value;
}

uint16_t  analog_pin::analog_write(uint16_t l_pin_value){
    pin_value = l_pin_value;
    # if LOG_LEVEL > LOG_LEVEL_NO
      cout << "LOG: Analog Pin WR:" << pin_name << " - ID: " << pin_id << " - Mode: " << (pin_mode == APIN_MODE_IN ? "IN" : "OUT") << " - Value: " << pin_value << endl;
    #endif
    return pin_value;
}
