#include "../include/main.h"

using namespace std;

int main (int argc, char** argv){

  int ret_value = STATUS_OK;
  int cycles = 2;

  board *r_pi_board = NULL;

  string board_vendor("Raspberry Pi");
  string board_model("3 rev B");
  string board_serial("00000000");

  r_pi_board = new board(0, board_vendor, board_model, board_serial);

  if(!r_pi_board->board_init()){
    ret_value = STATUS_ERROR;
  }

  if (ret_value == STATUS_OK){
    r_pi_board->digital_pin_add(0, string("PIN17"), DPIN_MODE_OUT, DVALUE_LOW);
    r_pi_board->digital_pin_add(1, string("PIN18"), DPIN_MODE_OUT, DVALUE_LOW);
    r_pi_board->digital_pin_add(2, string("PIN19"), DPIN_MODE_IN, DVALUE_LOW);
    r_pi_board->digital_pin_add(3, string("PIN20"), DPIN_MODE_IN, DVALUE_LOW);

    r_pi_board->digital_write(2, DVALUE_HIGH); //should produce an error (poin 2 is set as INPUT)

    r_pi_board->digital_pin_set_mode(2, DPIN_MODE_OUT);

    r_pi_board->digital_write(2, DVALUE_HIGH); //should be allowed

    while(cycles > 0){
      r_pi_board->digital_write(0, DVALUE_HIGH);
      r_pi_board->digital_write(1, DVALUE_LOW);
      r_pi_board->digital_read(0);
      r_pi_board->digital_read(1);
      sleep(1);
      r_pi_board->digital_write(0, DVALUE_LOW);
      r_pi_board->digital_write(1, DVALUE_HIGH);
      r_pi_board->digital_read(0);
      r_pi_board->digital_read(1);
      sleep(1);
      cycles--;
    }
  }

  delete r_pi_board;

  return ret_value;
}
