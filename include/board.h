#ifndef BOARD_H
  #define BOARD_H

// system status
  #define STATUS_ERROR     -1
  #define STATUS_OK         0

// digital pin mode
  #define DPIN_MODE_IN      0
  #define DPIN_MODE_OUT     1
// digital pin value
  #define DVALUE_LOW        0
  #define DVALUE_HIGH       1

// analog pin mode
  #define APIN_MODE_IN      0
  #define APIN_MODE_OUT     1
// analog pin value
  #define AREAD_ERROR       0
  #define AWRITE_ERROR      0
  #define AWRITE_OK         0

// libraries
  #include <iostream>
  #include <string>
  #include <map>
  #include "config.h"
#if DEMO != 1
  #include <wiringPi.h>
#endif

  using namespace std;

  /******************************************************************************/
  /*********************************BOARD I/O************************************/
  /******************************************************************************/

  class digital_pin {
      int16_t   pin_id = -1;
      string    pin_name = string("");
      uint8_t   pin_mode = DPIN_MODE_OUT;
      bool      pin_value = DVALUE_LOW;
    public:
      // const uint8_t DVALUE_LOW = 0;
      // const uint8_t DVALUE_HIGH = 1;
      // const uint8_t DPIN_MODE_IN = 0;
      // const uint8_t DPIN_MODE_OUT = 1;

      digital_pin(int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, bool l_pin_value);
      ~digital_pin (void);
      int16_t   get_pin_id(void);
      string    get_pin_name(void);
      uint8_t   get_pin_mode(void);
      uint8_t   set_pin_mode(uint8_t l_pin_mode);
      bool      digital_read(void);
      bool      digital_write(bool l_pin_value);
      bool      digital_toggle(void);
  };


  class analog_pin {
      int16_t   pin_id = -1;
      string    pin_name = string("");
      uint8_t   pin_mode = DPIN_MODE_OUT;
      uint16_t  pin_value = 0x0000;
    public:
      analog_pin(int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, uint16_t l_pin_value);
      ~analog_pin (void);
      int16_t   get_pin_id(void);
      string    get_pin_name(void);
      uint8_t   get_pin_mode(void);
      uint8_t   set_pin_mode(uint8_t l_pin_mode);
      uint16_t  analog_read(void);
      uint16_t  analog_write(uint16_t l_pin_value);
  };

  /******************************************************************************/
  /*************************************BOARD************************************/
  /******************************************************************************/

  class board {
      int board_id = -1;
      bool init_success   = false;
      string board_vendor = string("");
      string board_model  = string("");
      string board_serial = string("00000000");
      map<uint8_t, digital_pin*> digital_pins;
      map<uint8_t, analog_pin*>  analog_pins;
    public:
      board(int l_board_id, string l_board_vendor, string l_board_model, string l_board_serial);
      ~board (void);
      bool      board_init(void);
      int8_t    digital_pin_add(int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, uint8_t l_pin_value);
      int8_t    digital_pin_remove(int16_t l_pin_id);
      int8_t    digital_pin_set_mode(uint8_t l_pin_id, uint8_t l_pin_mode);
      uint8_t   analog_pin_add(int16_t l_pin_id, string l_pin_name, uint8_t l_pin_mode, uint16_t l_pin_value);
      uint8_t   analog_pin_remove(int16_t l_pin_id);
      int8_t    analog_pin_set_mode(uint8_t l_pin_id, uint8_t l_pin_mode);
      int8_t    digital_read(uint8_t l_pin_id);
      int8_t    digital_write(uint8_t l_pin_id, bool l_pin_value);
      int8_t    digital_toggle(uint8_t l_pin_id);
      uint16_t  analog_read(uint8_t l_pin_id);
      uint16_t  analog_write(uint8_t l_pin_id, uint16_t l_pin_value);
  };

#endif
